function getDetailsById(inventory, id) {
    if( (inventory) && (id) && (inventory.length>0) ){
                for (let i = 0; i < inventory.length; i++) {
                    if (inventory[i].id === id) {
                        return (inventory[i])
                    }
                }
    }
    else { return ([]) }
}
module.exports = getDetailsById;