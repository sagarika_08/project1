function getOlderCars(inventory,carYears){
    if((inventory) && (carYears) && (carYears.length>0) && (inventory.length>0)){
        let olderCars=[];
        for(let i=0;i<carYears.length;i++)
        {
            if(carYears[i]<2000){
                olderCars.push(inventory[i]);
            }
        }
        return olderCars;
    }
    else{
        return([])
    }
}
module.exports = getOlderCars;





// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, find out how many cars
// were made before the year 2000 and return the array of older cars and log its length.
 